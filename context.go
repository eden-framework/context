package context

import (
	"context"
	"reflect"
	"sync"
	"unsafe"
)

type WaitStopContext struct {
	ctx        context.Context
	cancelFunc context.CancelFunc
	wg         sync.WaitGroup
}

func NewWaitStopContext() *WaitStopContext {
	ctx, cancel := context.WithCancel(context.Background())
	return &WaitStopContext{
		ctx:        ctx,
		cancelFunc: cancel,
	}
}

func (c *WaitStopContext) Cancel() {
	c.cancelFunc()
	c.wg.Wait()
}

func (c *WaitStopContext) Add(delta int) {
	c.wg.Add(delta)
}

func (c *WaitStopContext) Finish() {
	c.wg.Done()
}

func (c *WaitStopContext) Done() <-chan struct{} {
	return c.ctx.Done()
}

func (c *WaitStopContext) Value(key interface{}) interface{} {
	return c.ctx.Value(key)
}

func WithValue(parent *WaitStopContext, key interface{}, value interface{}) {
	parent.ctx = context.WithValue(parent.ctx, key, value)
}

const (
	TempCancelContextKey = "temp_cancel_func"
)

func WithTempCancel(parent *WaitStopContext, cancelFunc context.CancelFunc) {
	parent.ctx = context.WithValue(parent.ctx, TempCancelContextKey, cancelFunc)
}

func (c *WaitStopContext) TempCancel() context.CancelFunc {
	return c.ctx.Value(TempCancelContextKey).(context.CancelFunc)
}

type iface struct {
	itab, data uintptr
}

type valueCtx struct {
	context.Context
	key, value any
}

func CopyContext(ctx context.Context) context.Context {
	newCtx := context.Background()
	kv := make(map[any]any)
	getKeyValues(ctx, kv)
	for k, v := range kv {
		newCtx = context.WithValue(newCtx, k, v)
	}

	return newCtx
}

func getKeyValues(ctx context.Context, kv map[any]any) {
	rtType := reflect.TypeOf(ctx).String()

	// 遍历到最底层，返回
	if rtType == "context.backgroundCtx" || rtType == "context.todoCtx" {
		return
	}

	ictx := *(*iface)(unsafe.Pointer(&ctx))

	if ictx.data == 0 {
		return
	}

	valCtx := (*valueCtx)(unsafe.Pointer(ictx.data))
	if valCtx != nil && valCtx.key != nil && valCtx.value != nil {
		kv[valCtx.key] = valCtx.value
	}

	getKeyValues(valCtx.Context, kv)
}
