package context

import (
	"context"
	"fmt"
	"testing"
)

func TestCopyContext(t *testing.T) {
	ctx := context.WithValue(context.Background(), "foo", "bar")
	cloneCtx := CopyContext(ctx)

	fmt.Println(cloneCtx.Value("foo"))
}
