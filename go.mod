module gitee.com/eden-framework/context

go 1.18

require (
	github.com/fatih/color v1.9.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.2.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
